class_name Ability extends Node2D

signal on_cooldown(time_left: float)
signal off_cooldown()
signal activate()

@export var ability_name: String

@export var cooldown: float =  1.0

@export_file("*.png", "*.jpg", "*.webp") var ability_icon
@export_file("*.tscn") var ability_event_scene

var ability_event: Resource

var is_on_cooldown: bool = false

@onready var cooldown_timer: Timer = $CooldownTimer

func _ready() -> void:
	cooldown_timer.wait_time = cooldown

	if ability_event_scene:
		ability_event = load(ability_event_scene)

func call_ability(player: Player, mouse_pos: Vector2) -> bool:
	if not is_on_cooldown:
		cooldown_timer.start()
		is_on_cooldown = true
		activate.emit()
		on_cooldown.emit(cooldown_timer.time_left)
		return true
	else:
		on_cooldown.emit(cooldown_timer.time_left)
		return false

func _on_cooldown_timer_timeout():
	is_on_cooldown = false
	off_cooldown.emit()
