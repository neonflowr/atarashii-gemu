class_name AbilitiesComponent
extends Node2D

func setup(player: Player) -> void:
	var ability_button_scene:= preload("res://src/templates/ability_button.tscn")

	var abilities: Array = get_children()
	for ability in abilities:
		# Add to the UI
		var ability_button_instance: AbilityButton = ability_button_scene.instantiate()
		player.ability_container.add_child(ability_button_instance)
		ability_button_instance.set_ability_name(ability.ability_name)
		ability_button_instance.set_ability_icon(ability.ability_icon)

		# Connect relevant signals
		ability.on_cooldown.connect(ability_button_instance.notify_on_cooldown)
		ability.off_cooldown.connect(ability_button_instance.notify_off_cooldown)
		ability.activate.connect(ability_button_instance.play_activate_anim)
