class_name AbilityButton
extends Control

@onready var ability_label: Label = $Label
@onready var texture: TextureRect = $TextureRect
@onready var anim: AnimationPlayer = $AnimationPlayer

func set_ability_name(ability_name: String) -> void:
	ability_label.text = ability_name

func set_ability_icon(path: String) -> void:
	var icon := load(path)
	texture.texture = icon

func notify_on_cooldown(time_left: float) -> void:
	anim.play("on_cooldown")

func notify_off_cooldown() -> void:
	anim.play("off_cooldown")

func play_activate_anim() -> void:
	anim.play("activate")
