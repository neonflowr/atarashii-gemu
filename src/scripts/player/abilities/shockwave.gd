extends Ability

func call_ability(player: Player, mouse_pos: Vector2) -> bool:
	if super(player, mouse_pos):
		print_debug("running main shockwave logic")
		var instance: AbilityEvent = self.ability_event.instantiate()
		var current_scene = get_tree().get_current_scene()
		if current_scene is BaseScene:
			current_scene.object_layer.add_child(instance)
		else:
			current_scene.add_child(instance)
		instance.activate(player, mouse_pos)
		return true
	else:
		return false
