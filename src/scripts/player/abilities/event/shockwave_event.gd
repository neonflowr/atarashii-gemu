extends AbilityEvent

@export var ACTIVATION_DELAY_TIME: float = 2

# Shockwave ability
@onready var pivot: Marker2D = $Pivot
@onready var vfx: Sprite2D = $Pivot/VFX
@onready var anim: AnimationPlayer = $AnimationPlayer

func activate(player: Player, mouse_pos: Vector2) -> void:
	# HACK: We do it like this because of Y-Sorting
	if mouse_pos.y > player.global_position.y:
		self.global_position = player.global_position
		pivot.look_at(mouse_pos)
	else:
		var point = player.global_position + player.global_position.direction_to(mouse_pos) * 640
		self.global_position = point
		# Sprite has to be at player pos
		self.pivot.global_position = player.global_position
		pivot.look_at(point)

	anim.play("activate")
	await get_tree().create_timer(ACTIVATION_DELAY_TIME).timeout
	anim.play("stop")
	await anim.animation_finished
	self.queue_free()
