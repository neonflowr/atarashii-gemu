class_name MouseTracer extends Sprite2D

@onready var timer: Timer = $Timer

var tracer_disabled: bool = false

func show_tracer() -> void:
	visible = true
	timer.start()

func hide_tracer() -> void:
	visible = false

func set_tracer_position(pos: Vector2) -> void:
	global_position = pos

func on_player_move(player: Player, mouse_pos: Vector2) -> void:
	show_tracer()
	set_tracer_position(mouse_pos)
	await timer.timeout
	hide_tracer()

func on_player_stop(player: Player, mouse_pos: Vector2) -> void:
	hide_tracer()

func tracer_enable() -> void:
	tracer_disabled = false

func tracer_disable() -> void:
	tracer_disabled = true
