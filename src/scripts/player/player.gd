class_name Player extends CharacterBody2D

signal setup_ready(player: Player)
signal first_ability(player: Player, mouse_pos: Vector2)
signal second_ability(player: Player, mouse_pos: Vector2)
signal third_ability(player: Player, mouse_pos: Vector2)
signal fourth_ability(player: Player, mouse_pos: Vector2)
signal moving_signal(player: Player, mouse_pos: Vector2)
signal stop_signal(player: Player, mouse_pos: Vector2)

@export var auto_set_camera_limit: bool = true

const MAX_SPEED: float = 300.0
const JUMP_VELOCITY: float = -400.0
const ACCELERATION: float = 1200

var move_direction
var current_speed: float = 300.0
var moving: bool = false
var destination: Vector2
var movement: Vector2

@onready var sprite: Sprite2D = $Sprite
@onready var camera: Camera2D = $Camera2D

@onready var ui: CanvasLayer = $CanvasLayer
@onready var ability_container: HBoxContainer = ui.get_node("Control/BoxContainer")

func _ready() -> void:
	# Set camera limit according to level's type and size
	if auto_set_camera_limit:
		# HACK: Must use get_node() with stringly typed names instead of using GameScene's `map` property here unfortunately
		var map = get_tree().current_scene.get_node("BG/Map")

		if map is TileMap:
			var x = map.get_used_rect().size.x
			var y = map.get_used_rect().size.y
			camera.limit_right = map.get_used_rect().size.x * map.scale.x * map.tile_set.tile_size.x
			camera.limit_bottom = map.get_used_rect().size.y * map.scale.y * map.tile_set.tile_size.y
		elif map is Sprite2D:
			camera.limit_right = map.get_texture().get_size().x * map.scale.x
			camera.limit_bottom = map.get_texture().get_size().y * map.scale.y
	setup_ready.emit(self)

func movement_loop(delta):
	if not moving:
		pass
	else:
		current_speed += ACCELERATION * delta
		if current_speed > MAX_SPEED:
			current_speed = MAX_SPEED

		movement = position.direction_to(destination) * current_speed
		move_direction = rad_to_deg(destination.angle_to_point(position))

		if position.distance_to(destination) > 5:
			velocity.x = movement.x
			velocity.y = movement.y
			move_and_slide()
		else:
			stop_moving()

func stop_moving() -> void:
	velocity.x = 0
	velocity.y = 0
	movement = Vector2(0, 0)
	moving = false
	destination = position

func _physics_process(delta):
	if Input.is_action_just_pressed("stop"):
		stop_moving()
		stop_signal.emit(self, get_global_mouse_position())
	elif Input.is_action_just_pressed("move"):
		moving = true
		destination = get_global_mouse_position()
		moving_signal.emit(self, get_global_mouse_position())

	if Input.is_action_just_pressed("first_ability"):
		first_ability.emit(self, get_global_mouse_position())
	elif Input.is_action_just_pressed("second_ability"):
		second_ability.emit(self, get_global_mouse_position())
	elif Input.is_action_just_pressed("third_ability"):
		third_ability.emit(self, get_global_mouse_position())
	elif Input.is_action_just_pressed("fourth_ability"):
		fourth_ability.emit(self, get_global_mouse_position())

	movement_loop(delta)

	if velocity.x < 0:
		sprite.flip_h = true
	elif velocity.x > 0:
		sprite.flip_h = false
